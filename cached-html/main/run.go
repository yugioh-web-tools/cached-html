package main

import (
	"io/ioutil"
	"log"
	"main/cachedhttp"
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
)

func main() {
	engine := gin.Default()

	engine.GET("/", root)

	engine.Run(":8000")
}

func root(c *gin.Context) {
	q := c.Query("q")
	if q == "" {
		c.String(400, "q is required query string")
		return
	}
	targetUrl, err := url.PathUnescape(q)
	if err != nil {
		log.Println(err)
		c.String(400, "can not parse query")
		return
	}

	data, err := cachedhttp.Get(http.Get, "/cache_data", targetUrl)
	if err != nil {
		log.Println(err)
		c.String(400, "query url is not valid")
		c.String(400, "\nif you want access example.com")
		c.String(400, "\nq=http%3A%2F%2Fexample.com%2F")
		return
	}

	byteData, err := ioutil.ReadAll(data)
	if err != nil {
		log.Println(err)
		c.String(500, "server err")
		return
	}
	c.String(200, string(byteData))
}
