package cachedhttp

import (
	"io"
	"net/http"
)

func Get(
	client func(url string) (resp *http.Response, err error),
	cacheDir string, url string) (data io.Reader, err error) {

	if hit, _ := ReadCache(cacheDir, url); !hit {
		resp, err := client(url)
		if err != nil {
			return nil, err
		}
		data = resp.Body
		WriteCache(cacheDir, url, data)
	}

	_, file := ReadCache(cacheDir, url)
	return file, nil
}
