package cachedhttp

import (
	"io"
	"io/ioutil"
	"net/url"
	"os"
)

func cachePath(cacheDir string, reqUrl string) string {
	encodedUrl := url.PathEscape(reqUrl)
	if _, err := os.Stat(cacheDir); os.IsNotExist(err) {
		os.Mkdir(cacheDir, 666)
	}
	return cacheDir + "/" + encodedUrl
}

func ReadCache(cacheDir string, reqUrl string) (hit bool, data *os.File) {
	if file, err := os.Open(cachePath(cacheDir, reqUrl)); err == nil {
		return true, file
	}
	return false, nil
}

func WriteCache(cacheDir string, reqUrl string, data io.Reader) error {
	byteData, err := ioutil.ReadAll(data)
	if err != nil {
		return err
	}
	ioutil.WriteFile(cachePath(cacheDir, reqUrl), byteData, os.ModePerm)
	return nil
}
