package cachedhttp

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
)

func TestGetFromInternet(t *testing.T) {
	// setup test
	dammyClient := func(url string) (*http.Response, error) {
		return &http.Response{
			StatusCode:       200,
			Body:             ioutil.NopCloser(bytes.NewBufferString("test")),
		}, nil
	}
	pwd, err := os.Getwd()
	if err != nil {
		t.Fatal("")
	}
	testUrl := "dammy.faketld/not-exists.html"
	testCacheDir := pwd + "/client_test"
	testCachePath := testCacheDir + "/dammy.faketld%2Fnot-exists.html"
	os.Remove(testCachePath)

	// exec test target func
	data, err := Get(dammyClient, testCacheDir, testUrl)
	if err != nil {
		t.Fatal("")
	}
	// test request body
	dataString, err := ioutil.ReadAll(data)
	if err != nil {
		t.Fatal("")
	}
	if !bytes.Equal(dataString, []byte("test")) {
		t.Fatal(dataString)
	}

	// test writting cache
	cacheFile, err := os.Open(testCachePath)
	if os.IsNotExist(err) {
		t.Fatal("")
	}
	cacheData, err := ioutil.ReadAll(cacheFile)
	if err != nil {
		t.Fatal("")
	}
	if err != nil {
		t.Fatal("")
	}
	if !bytes.Equal(cacheData, []byte("test")) {
		t.Fatal("")
	}

	// teardown test
	err = os.Remove(testCachePath)
	if err != nil {
		t.Fatal("")
	}	
}

func TestGetFromCache(t *testing.T) {
	// setup test
	dammyClient := func(url string) (*http.Response, error) {
		return nil, nil
	}
	pwd, err := os.Getwd()
	if err != nil {
		t.Fatal("")
	}
	testUrl := "dammy.faketld/index.html"
	testCacheDir := pwd + "/client_test"

	// exec test target func
	data, err := Get(dammyClient, testCacheDir, testUrl)
	if err != nil {
		t.Fatal("")
	}

	// test request body
	dataString, err := ioutil.ReadAll(data)
	if err != nil {
		t.Fatal("")
	}
	if !bytes.Equal(dataString, []byte("dammy data\n")) {
		t.Fatal("")
	}
}
