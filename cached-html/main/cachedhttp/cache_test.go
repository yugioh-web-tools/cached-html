package cachedhttp

import (
	"bytes"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestAll(t *testing.T) {
	// setup test
	pwd, err := os.Getwd()
	if err != nil {
		t.Fatal("")
	}
	testCacheDir := pwd + "/cache_test"
	testCachePath := testCacheDir + "/a%2Fb%3Fc&d%25e"
	testUrl := "a/b?c&d%e"
	testData := "testdata"
	os.Remove(testCachePath)

	// write cache
	WriteCache(testCacheDir, testUrl, strings.NewReader(testData))

	// read cache
	hit, data := ReadCache(testCacheDir, testUrl)
	if !hit {
		t.Fatal("")
	}
	dataString, err := ioutil.ReadAll(data)
	if err != nil {
		t.Fatal("")
	}
	if !bytes.Equal(dataString, []byte(testData)) {
		t.Fatal("")
	}

	// teardown
	os.Remove(testCachePath)
}
