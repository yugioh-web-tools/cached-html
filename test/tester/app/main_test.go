package app

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
)

func TestAll(t *testing.T) {
	targetUrl := url.PathEscape("http://server/index.html")
	resp, err := http.Get("http://cached-html?q=" + targetUrl)
	if err != nil {
		t.Fatal(err)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}
	if string(data) != "test\n" {
		t.Fatal("read data is not correct : " + string(data))
	}
}
