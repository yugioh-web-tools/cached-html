# 概要
html取得の際にキャッシュを使えるようにするサービスです。

`/cachedhtml_cache`にキャッシュを置きます。必要に応じて永続化してください。

# API
http://hostname?q={url}

* url[必須] : パーセントエンコーディングされたurl
