#!/bin/bash -e

cd $(dirname $0)

echo -n "setup cluster..."
sed -e "s@CACHED_HTML_IMAGE@$CACHED_HTML_IMAGE@g" kubernetes.yml | kubectl create -f -
sed -e "s@TEST_TESTER_IMAGE@$TEST_TESTER_IMAGE@g" ./test/tester.yml | kubectl create -f -
sed -e "s@TEST_SERVER_IMAGE@$TEST_SERVER_IMAGE@g" ./test/server.yml | kubectl create -f -
echo " done"

echo "wait creating pods"
while [ $(kubectl get pods --field-selector status.phase=Running --no-headers | wc -l) != "3" ]; do
    sleep 0.1
    echo -n "."
done
echo "done"

echo "Test : get"
kubectl exec -i $(kubectl get po -o custom-columns=:metadata.name -l app=tester --no-headers) \
	-- sh -c "CGO_ENABLED=0 go test ./... -count=1"
echo "done"

echo "down html server for cache test..."
sed -e "s@TEST_SERVER_IMAGE@$TEST_SERVER_IMAGE@g" ./test/server.yml | kubectl delete -f -
while [ $(kubectl get pods --field-selector status.phase=Running --no-headers | wc -l) != "2" ]; do
    sleep 0.1
    echo -n "."
done
echo "done"

echo "Test : get from cached"
kubectl exec -i $(kubectl get po -o custom-columns=:metadata.name -l app=tester --no-headers) \
	-- sh -c "CGO_ENABLED=0 go test ./... -count=1"
echo "done"
